springcloud 基础项目

server-springcloud  :8761 Eureka 注册中心

serverTwo-springcloud  :8762 Eureka 注册中心 高可用注册中心 其实就是注册中心的相互注册，数据互通 需要改host  127.0.0.1 peer1   127.0.0.1 peer2 

client-springcloud  ：8763 提供者 日常api服务，某个模块

两个不同风格的消费者
feign-springcloud   ：8765 消费者 feign 提供接口暴露等

ribbon-springcloud  : 8764 消费者，功能同feign

zuul-springcloud    : 8769 网关 类似nginx

config-springcloud  : 8768 配置文件中心

由于版本以来的问题，本项目均采用<version>2.0.3.RELEASE</version>，不会出现版本问题

    
    其实每个服务 都 既扮演消费者 也扮演提供者，都是通过service@feign的方式服务发现的
    
    spring cloud的Netflix中提供了两个组件实现软负载均衡调用：ribbon和feign。
    
    Ribbon 
    是一个基于 HTTP 和 TCP 客户端的负载均衡器 
    它可以在客户端配置 ribbonServerList（服务端列表），然后轮询请求以实现均衡负载。
    
    Feign 
    Spring Cloud Netflix 的微服务都是以 HTTP 接口的形式暴露的，
    所以可以用 Apache 的 HttpClient 或 Spring 的 RestTemplate 去调用，
    而 Feign 是一个使用起来更加方便的 HTTP 客戶端，使用起来就像是调用自身工程的方法，而感觉不到是调用远程方法。
    注意：spring-cloud-starter-feign 里面已经包含了 spring-cloud-starter-ribbon（Feign 中也使用了 Ribbon）



关于config 
    目前使用的是保存在git的方式，也可以使用数据库的保存方式
    内部默认端口是8888，所以需要创建bootstrap文件，显式赋值 https://www.cnblogs.com/ANCAN-RAY/p/8795448.html




http://localhost:8765/hi?name="12313" 会调用到8763
http://localhost:8764/hi?name="12313" 会调用到8763

http://localhost:8769/api-a/hi?name="12313" 会转发到 service-ribbon  会调用到8763
http://localhost:8769/api-b/hi?name="12313" 会转发到 service-feign  会调用到8763